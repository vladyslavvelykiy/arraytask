package com.epam;

import com.epam.ABC.A;
import com.epam.ABC.B;
import com.epam.ABC.C;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {
    static final Logger logger = LogManager.getLogger(App.class.getName());

    public static void main(String[] args) {

        //Task A:
        logger.info("The third array will contain the first two");// a)
        logger.info("Write the sizes of the arrays: ");
        A a = new A();
        logger.info(a.containsElOfBothArrs());
        logger.info(a.toString());
        logger.info("The third array will contains of elements that are in only one of the arrays: ");//b)
        a.doXOR();
        logger.info(a.toString());

        //Task B
        logger.info("Remove all numbers that are repeated more than twice from the array");
        B b = new B ();
        logger.info("Write the size of the array: ");
        logger.info(b.fillArray());
        b.doNoMoreTwo();
        logger.info(b.toString());

        //Task C
        logger.info("Find in the array all series of identical elements that go in a row and delete " +
                "all elements in them except one. Type the length of elements");
        logger.info("Write the size of array: ");
        C c = new C();
        logger.info(c.fillArray());
        c.writeNoRepeats();
        logger.info(c.toString());




    }
}
