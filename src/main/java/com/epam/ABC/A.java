package com.epam.ABC;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class A {
    private Random rand;
    private Scanner scan;
    private int length;
    private int[] array;
    private int[] firstArr;
    private int[] secondArr;

    public A() {
        rand = new Random();
        scan = new Scanner(System.in);
        firstArr = fillArray();
        secondArr = fillArray();
        length = firstArr.length + secondArr.length;
    }


    public String containsElOfBothArrs() {

        int i = 0;
        array = new int[length];
        for (int e : firstArr) {
            array[i] = e;
            i++;
        }
        for (int e : secondArr) {
            array[i] = e;
            i++;
        }
        return "1st array: " + Arrays.toString(firstArr) + "\n" + "2nd array: " + Arrays.toString(secondArr);
    }//A a)

    public void doXOR() {
        int[] newArray;
        int newLength;
        int i = 0;
        array = new int[length];
        newLength = firstArr.length + secondArr.length;

        for (int e : firstArr) {
            if (contains(secondArr, e)) {
                newLength--;
            } else {
                array[i] = e;
                i++;
            }
        }
        for (int e : secondArr) {
            if (contains(firstArr, e)) {
                newLength--;
            } else {
                array[i] = e;
                i++;
            }
        }
        newArray = new int[newLength];
        for (int k = 0, j = 0; j < newLength; k++, j++) {
            newArray[j] = array[k];
        }
        array = newArray;
    }//A б)

    private int[] fillArray() {
        int[] array = new int[scan.nextInt()];
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt(10);
        }
        return array;
    }

    private boolean contains(int[] array, int el) {
        for (int e : array) {
            if (e == el) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "3d Array  " + Arrays.toString(array);
    }
}
