package com.epam.ABC;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class B implements ArrayPrintable {
    private Random rand;
    private Scanner scan;
    private int[] array;
    private int[] newArr;

    public B() {
        rand = new Random();
        scan = new Scanner(System.in);
    }

    public void doNoMoreTwo() {
        newArr = new int[findSizeArr()];
        int j = -1;
        for (int i = 0; i < newArr.length; i++) {
            while (j < array.length) {
                j++;
                if (isUnique(array, array[j])) {
                    newArr[i] = array[j];
                    break;
                }
            }
        }

    }

    @Override
    public String toString() {
        return "newArr = " + Arrays.toString(newArr);
    }

    public String fillArray() {
        array = new int[scan.nextInt()];
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt(10);
        }

        return "Generated array: " + Arrays.toString(array);
    }

    private int findSizeArr() {
        int length = 0;
        for (int e : array) {
            if (isUnique(array, e)) {
                length++;
            }
        }
        return length;
    }

    private boolean isUnique(int[] array, int el) {
        int counter = 0;
        for (int e : array) {
            if (el == e) {
                counter++;
            }
        }
        if (counter <= 2) {
            return true;
        } else
            return false;

    }


}
