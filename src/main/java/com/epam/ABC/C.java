package com.epam.ABC;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class C implements ArrayPrintable{
    private Random rand;
    private Scanner scan;
    private int[] array;
    private int[] newArr;
    private int length;

    public C() {
        rand = new Random();
        scan = new Scanner(System.in);
    }


    public int[] writeNoRepeats() {
        length = array.length;
        for (int i = 0; i < array.length; i++) {
            if (isRepeat(i)) {
                do {
                    length--;
                    this.newArr = fillWithoutElement(newArr, i);
                    array = newArr;
                } while (isRepeat(i));
            }
        }
        return this.newArr;
    }


    public String fillArray() {
        array = new int[scan.nextInt()];
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt(10);
        }
        return "Generated array " +  Arrays.toString(array);
    }


    private int[] fillWithoutElement(int[] newArr, int place) {
        newArr = new int[length];
        for (int i = 0; i <array.length+1;i++){
            if(i<newArr.length){
                if(i>=place){
                    newArr[i] = array[i+1];
                }else{
                    newArr[i] = array[i];
                }
            }
        }
    return newArr;
    }

    private boolean isRepeat(int place) {
        try{
            if(array[place]==array[place+1]){
                return true;
            }
            return false;
        }catch (ArrayIndexOutOfBoundsException e){
            return false;
        }

    }


    @Override
    public String toString() {
        return "newArr = " + Arrays.toString(newArr);
    }

}
